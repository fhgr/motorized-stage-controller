import ctypes as ct
import time


class EposWrapper:
    
    def __init__(self):
        pass

    def _handle_error(self, error_code):
        
        if error_code.value != 0:
            self.setDisableState()
            raise Exception(f'Maxon EPOS error code {error_code.value:x}')
            
    def open(self):
        # Load DLL
        library_path = './EposCmd64'
        ct.cdll.LoadLibrary(library_path)
        self.epos = ct.CDLL(library_path)
        # Initialize library
        pErrorCode = ct.c_uint()
        self._key_handle = self.epos.VCS_OpenDevice(b'EPOS4', b'MAXON SERIAL V2', b'USB', b'USB0', ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        baudrate = 1000000
        timeout = 500
        self.epos.VCS_SetProtocolStackSettings(self._key_handle, baudrate, timeout, ct.byref(pErrorCode)) # set baudrate
        self._handle_error(pErrorCode)
        self._node_id = 12
        self.epos.VCS_ClearFault(self._key_handle, self._node_id, ct.byref(pErrorCode)) # clear all faults
        self._handle_error(pErrorCode)
        # self.epos.VCS_ActivateProfilePositionMode(self._key_handle, self._node_id, ct.byref(pErrorCode)) # activate profile position mode
        # self._handle_error(pErrorCode)
        # self.epos.VCS_SetEnableState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # enable device
        # self._handle_error(pErrorCode)
   
  
   
    def close(self):
        pErrorCode = ct.c_uint()
        self.epos.VCS_SetDisableState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # disable device
        self._handle_error(pErrorCode)
        self.epos.VCS_CloseDevice(self._key_handle, ct.byref(pErrorCode)) # close device
        self._handle_error(pErrorCode)
        
        
        
    def getPositionIs(self):
        pPositionIs = ct.c_long()
        pErrorCode = ct.c_uint()
        self.epos.VCS_GetPositionIs(self._key_handle, self._node_id, ct.byref(pPositionIs), ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        return pPositionIs.value
    
    def setDisableState(self):
        pErrorCode = ct.c_uint()
        self.epos.VCS_SetDisableState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # disable device
        self._handle_error(pErrorCode)
        
        
    def setQuickStopState(self):
        pErrorCode = ct.c_uint()
        self.epos.VCS_SetQuickStopState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # set quick stop state
        self._handle_error(pErrorCode)
            
    
    def activateProfilePositionMode(self):
        pErrorCode = ct.c_uint()
        self.epos.VCS_ActivateProfilePositionMode(self._key_handle, self._node_id, ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        self.epos.VCS_SetEnableState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # enable device
        self._handle_error(pErrorCode)
    
    def moveToPositionSpeed(self, target_position, target_speed):
        acceleration = 1000 # rpm/s, up to 1e7 would be possible 50000
        deceleration = 1000 # rpm/s 50000
        pErrorCode = ct.c_uint()
        true_position = self.getPositionIs()
        #while True:
        if target_speed != 0:
           self.epos.VCS_SetPositionProfile(self._key_handle, self._node_id, target_speed,
                                            acceleration, deceleration, ct.byref(pErrorCode)) # set profile parameters
           self._handle_error(pErrorCode)
           self.epos.VCS_MoveToPosition(self._key_handle, self._node_id, target_position, True, True, ct.byref(pErrorCode)) # move to position
           self._handle_error(pErrorCode)
           
        elif target_speed == 0:
           self.epos.VCS_HaltPositionMovement(self._key_handle, self._node_id, ct.byref(pErrorCode)) # halt motor
        true_position = self.getPositionIs()
    #   if true_position == target_position:
     #      break
            
    def waitForTargetReached(self):
        timeout=20000
        pErrorCode = ct.c_uint()
        self.epos.VCS_WaitForTargetReached(self._key_handle, self._node_id, timeout, ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        
    def waitForHomingAttained(self):
        timeout=20000
        pErrorCode = ct.c_uint()
        self.epos.VCS_WaitForHomingAttained(self._key_handle, self._node_id, timeout, ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        
            
    def getAnalogInput(self, input_number):
        analogInputValue=ct.c_uint() #or c_double()) ??
        pErrorCode = ct.c_uint()
        self.epos.VCS_GetAnalogInput(self._key_handle, self._node_id, input_number, ct.byref(analogInputValue), ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        return analogInputValue.value
    
    def getAnalogInputVoltage(self, input_number):
        analogInputVoltage=ct.c_long() #or c_double()) ??
        pErrorCode = ct.c_uint()
        self.epos.VCS_GetAnalogInputVoltage(self._key_handle, self._node_id, input_number, ct.byref(analogInputVoltage), ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        return analogInputVoltage.value


    def setHomingParameter(self, currentThreshold, homePosition, homingSpeed, indexSearchSpeed):
        homingAcceleration = 1000       #for EC flat 60
        homeOffset = 0#455
        pErrorCode = ct.c_uint()
        self.epos.VCS_SetHomingParameter(self._key_handle, self._node_id, homingAcceleration, homingSpeed, indexSearchSpeed, homeOffset, currentThreshold, homePosition, ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        
        
    def findHome(self, homingMode):
        print(f"Homing mode is {homingMode}")
        pErrorCode = ct.c_uint()
        self.epos.VCS_FindHome(self._key_handle, self._node_id, homingMode, ct.byref(pErrorCode))
        self.waitForHomingAttained()     #is this the correct way to do it?
        self._handle_error(pErrorCode)
               
        
    def activateHoming(self):
        pErrorCode = ct.c_uint()
        self.epos.VCS_ActivateHomingMode(self._key_handle, self._node_id, ct.byref(pErrorCode))
        self._handle_error(pErrorCode)
        self.epos.VCS_SetEnableState(self._key_handle, self._node_id, ct.byref(pErrorCode)) # enable device
        self._handle_error(pErrorCode)
        
        
        
        