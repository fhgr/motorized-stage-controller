# -*- coding: utf-8 -*-
"""
Created on Thu Mar  2 15:55:00 2023

@author: birkudo
"""

import sys
import os
import numpy as np
from PyQt5.QtCore import pyqtSignal, QObject
from rotDiscV03 import stageUB
import time

# https://www.pythonguis.com/tutorials/pyqt-basic-widgets/
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QComboBox,
    QDateEdit,
    QDateTimeEdit,
    QDial,
    QDoubleSpinBox,
    QFontComboBox,
    QLabel,
    QLCDNumber,
    QLineEdit,
    QMainWindow,
    QProgressBar,
    QPushButton,
    QRadioButton,
    QSlider,
    QSpinBox,
    QTimeEdit,
    QVBoxLayout,
    QHBoxLayout,
    QWidget,
    QStatusBar,
)

DEBUG = False
def debug(msg,value=None):
    if not DEBUG:
        return
    if value is None:
        print(msg)
    else:
        print(msg,value)

class EditWidget(QDoubleSpinBox):
    def __init__(self, parent=None, text='0', minimum = 0, maximum = 2048, step = 8):
        QDoubleSpinBox.__init__(self, parent=parent)
        self.parent = parent
        self.setStyleSheet("background-color: transparent; border: 0px ;")
        self.setMinimum(int(minimum))
        self.setMaximum(int(maximum))
        self.setDecimals(0)
        self.setSingleStep(int(step))
        # self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setValue(int(text))
        # self.editingFinished.connect(parent.on_edit_done)

# %%
# define signals to be send to the stage:
class Communicate(QObject):
    cancelSignal  = pyqtSignal()       # cancel button pressed
    terminateSignal = pyqtSignal()     # quit application (close stage)
    
# %%
# Subclass QMainWindow to customize your application's main window
class MainWindow(QMainWindow):
    pos = 0
    step = 8
    maxPos = 2048
    stage = None
    
    def __init__(self, stage = None):
        super().__init__()

        self.setWindowTitle("Rotation Stage GUI v02 (UB)")

        self.stage = stageUB()
        self.stage.start()
        self.maxPos = self.stage.rotDisc.resEncoder
        self.step = self.stage.steps_per_move
        self.pos = self.stage.rotDisc.counter

        layout = QVBoxLayout()
        layoutSteps = QHBoxLayout()
        layoutScan  = QHBoxLayout()
        
        self.moveOnDial = False

        self.dial = QDial(self, 
                          notchesVisible=True,
                          wrapping=True,)
        self.dial.setRange(0, self.maxPos)
        self.dial.setSingleStep(self.step)
        layout.addWidget(self.dial)
        self.dial.valueChanged.connect(self.on_dial_value_changed)
        self.dial.sliderMoved.connect(self.on_dial_slider_position)        

        self.back = QPushButton("back", self)
        self.LCD  = EditWidget(self, '0', 0, self.maxPos, self.step)
        self.fwd  = QPushButton("fwd", self)
        layoutSteps.addWidget(self.back)
        layoutSteps.addWidget(self.LCD)
        layoutSteps.addWidget(self.fwd)
        widgetSteps = QWidget()
        widgetSteps.setLayout(layoutSteps)
        layout.addWidget(widgetSteps)
        self.LCD.valueChanged.connect(self.on_LCD_value_changed)
        # self.LCD.textChanged.connect(self.on_LCD_text_changed)
        self.back.clicked.connect(self.on_back)
        self.fwd.clicked.connect(self.on_fwd)

        self.home  = QPushButton("home", self)
        self.home.clicked.connect(self.on_home)
        self.start = QPushButton("start", self)
        self.start.clicked.connect(self.on_start)
        self.cancel  = QPushButton("cancel", self)
        self.cancel.clicked.connect(self.on_cancel)
        layoutScan.addWidget(self.home)
        layoutScan.addWidget(self.start)
        layoutScan.addWidget(self.cancel)
        widgetScan = QWidget()
        widgetScan.setLayout(layoutScan)
        layout.addWidget(widgetScan)
        
        actionLabel = QLabel("action:", self)
        self.actionReport = QLineEdit(self)
        self.actionReport.setReadOnly(True)   # can still select text
        self.actionReport.setEnabled(False)   # can't do anything (grayed out)
        self.busyLabel = QLabel(self)
        self.busyLabel.resize(30, 30)
        # self.busyLabel.setStyleSheet("QLabel{margin-left: 10px; border-radius: 25px; background: red; color: #4A0C46;}")
        # self.busyLabel.setStyleSheet("border: 3px solid blue; border-radius: 40px;")
        self.busyLabel.setStyleSheet( "border: 1px solid #0000FF; background-color: yellow; border-radius: 50px;" )
        self.busyLabel.setAlignment( Qt.AlignCenter )
        layoutAction = QHBoxLayout()
        layoutAction.addWidget(actionLabel)
        layoutAction.addWidget(self.actionReport)
        layoutAction.addWidget(self.busyLabel)
        widgetReport = QWidget()
        widgetReport.setLayout(layoutAction)
        layout.addWidget(widgetReport)

        widget = QWidget()
        widget.setLayout(layout)

        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
      
        # Set the central widget of the Window. Widget will expand
        # to take up all the space in the window by default.
        self.setCentralWidget(widget)
        
        self.signals = Communicate()
        self.stage.registerSignals(self.reportProgress,None,self.setListeningState,self.signals)
        
        self.statusMsg("initializing ...")
        self.stage.command("home")

        
    def setListeningState(self, isListeing):
        if isListeing:
            self.busyLabel.setStyleSheet( "border: 1px solid #0000FF; background-color: #80C342; border-radius: 50px;" )
            self.enable(True)
        else:
            self.busyLabel.setStyleSheet( "border: 1px solid #0000FF; background-color: red; border-radius: 50px;" )

    def setStage(self, stage = None):
        self.stage = stage
    
    def enable(self, active=True):
        wids = [self.dial, self.back, self.LCD, self.fwd, self.home, self.start]
        for w in wids:
            w.setEnabled(active)
        # if active:
        # else:
        #     for w in wids:
        #         w.setDiabled()
                
    def update(self, pos = None):
        if pos is not None:
            pos = int(np.round(pos/self.step)*self.step)
            pos = np.mod(pos, self.maxPos)
            self.pos = pos
        else:
            pos = self.pos
        debug("pos:",pos)
        self.dial.setValue(pos)
        self.LCD.setValue(pos)
        
    def statusMsg(self, txt, timeout = 1000):
        self.statusBar.showMessage(txt,timeout)
        
    def on_dial_value_changed(self, i):
        debug("dial value:",i)
        if self.moveOnDial:
            self.update(i)
        else:
            pos = self.pos
            self.dial.setValue(pos)

    def on_dial_slider_position(self, i):
        debug("dial slider:",i)
        if self.moveOnDial:
            self.update(i)
        else:
            pos = self.pos
            self.dial.setValue(pos)

    def on_LCD_value_changed(self, i):
        debug("LCD value:",i)
        if self.moveOnDial:
            self.update(i)
        else:
            pos = self.pos
            self.LCD.setValue(pos)

    def on_LCD_text_changed(self, s):
        if self.moveOnDial:
            try:
                i = int(s)
                debug("LCD text:",i)
                self.update(i)
            except:
                pass
        else:
            pos = self.pos
            self.LCD.setValue(pos)
        
    def on_back(self):
        # if self.pos >= self.step:
        #     self.update(self.pos-self.step)
        self.stage.command("bwd")


    def on_fwd(self):
        # if self.pos <= self.maxPos-self.step:
        #     self.update(self.pos+self.step)
        self.stage.command("fwd")
                        
    def on_home(self):
        self.statusMsg("homing stage ...", 5000)
        self.enable(False)
        self.stage.command("home")
        
    def on_start(self):
        self.statusMsg("homing, then scanning ...", 5000)
        self.enable(False)
        self.stage.command("scan")
        
    def on_cancel(self):
        self.signals.cancelSignal.emit()

    def closeEvent(self,event):
        self.closeFigures()
        
    def closeFigures(self, closeMainWin=True):
        self.statusMsg("closing ...", 2000)
        self.statusBar.repaint()
        time.sleep(0.05)
        self.stage.close()
        if(closeMainWin):
            self.close()
        QApplication.quit()

    def reportCancel(self):
        print("Cancel button pressed.", flush=True)
        
    def reportProgress(self, i):
        self.actionReport.setText(f"pos ={i:4d} ({self.stage.num_moves})")
        self.update(i*self.step)


if __name__=="__main__":
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 

    window = MainWindow()
    window.show()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')
        window.deleteLater()
