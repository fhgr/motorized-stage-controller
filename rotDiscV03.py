from epos_wrapper import EposWrapper
import time
import numpy as np
import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
from itertools import count
# import threading
from threading import Thread
from PyQt5.QtCore import pyqtSignal
import cv2


LARGE_FONT= ("Verdana", 12)
NORM_FONT= ("Verdana", 10)
SMALL_FONT= ("Verdana", 8)

def popupmsg(msg):
    popup = tk.Tk()
    popup.wm_title("!!Manual Action necessary!!")
    label = ttk.Label(popup, text=msg, font=LARGE_FONT)
    label.pack(side="top", fill="x", pady=10)
    B1 = ttk.Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()
    

class rotDiscDummy():
    def __init__(self):
        self.resEncoder = 2048          #resolution of encoder 512imps/rot --> 2048 counts/rot
        self.counter = 0
        self.homePosition = 0
        self.setMotorPosition = self.homePosition
        
    def moveStep(self, stepSize):
        self.setMotorPosition=self.homePosition+self.counter*stepSize

    def getMotorPosition(self):
        return self.setMotorPosition

    def close(self):
        pass
    
class rotDisc:
    def __init__(self):
        ## Mechanical parameters of Hip Module and component parameters
        self.resEncoder = 2048          #resolution of encoder 512imps/rot --> 2048 counts/rot
        ## Motor Speed settings
        self.motSpeed=60#4500
        self.homingSpeed = 20
        self.indexSearchSpeed = 10        
        ## Parameter initialization
        self.stepSize=6 

                
    def _handle_error(self, error_code):
       pass
        
   
    def moveStep(self,stepSize): #  #increments per step --> 1 inc = 360°/resEncoder
                         
        self.setMotorPosition=self.homePosition+self.counter*stepSize
        self.goToSetMotorPosition()
        #ew.waitForTargetReached()
        
        
    def goToSetMotorPosition(self):
        self.ew.moveToPositionSpeed(self.setMotorPosition,self.motSpeed)
        self.ew.waitForTargetReached()
  
    def init(self, ew):
        self.currentThreshold = 1000
        self.homingMode = 37      #set actual position as home position
        self.homePosition = 0
        self.counter=0
        self.ew = ew
        self.ew.activateHoming()
        self.ew.setHomingParameter(self.currentThreshold, self.homePosition, self.homingSpeed, self.indexSearchSpeed)
        self.ew.findHome(self.homingMode)
        self.ew.activateProfilePositionMode()
        self.setMotorPosition=self.homePosition
        self.goToSetMotorPosition()
        # popupmsg("Homing Done! Press <OK> to start\n")
        return
    
    def getMotorPosition(self):
        return self.setMotorPosition
    
    def close(self):
        self.ew.close()
     

# %%
from PyQt5.QtCore import QObject

class stageUB(Thread, QObject):
    commandFinished  = pyqtSignal()       # single command done
    progress  = pyqtSignal(int)    # stage position
    listening = pyqtSignal(bool)   # all commands executed
    def __init__(self):
        # super().__init__()
        Thread.__init__(self)
        QObject.__init__(self)
        
        self.queue = []

        self.keep_running = False
        self.cancelSignal = False
        self.useEwStage   = False

        self.ew = EposWrapper()
        try:
            self.ew.open()
            self.rotDisc = rotDisc()      
            self.rotDisc.init(self.ew)
            self.useEwStage = True
        except:
            print("Hardware not found. Using dummy stage.", flush=True)
            self.rotDisc = rotDiscDummy()
        
        self.delay = 2   # 2 sekunden einschwingzeit
        
        steps_per_revolution = self.rotDisc.resEncoder   # 2048 steps in 360°
        
        self.steps_per_move = 8  # muss 2^n sein
        self.num_moves = steps_per_revolution // self.steps_per_move
        assert(self.num_moves*self.steps_per_move == steps_per_revolution)
        
        self.registerSignals(self.reportProgress, 
                             self.reportFinished, 
                             self.reportListening)
        
    def registerSignals(self, progressCallback, finishedCallback, listeningCallback, GUI=None):
        if progressCallback is not None:
            self.progress.connect(progressCallback)
        if finishedCallback is not None:
            self.commandFinished.connect(finishedCallback)
        if listeningCallback is not None:
            self.listening.connect(listeningCallback)
        if GUI is not None:
            GUI.cancelSignal.connect(self.cancelScan)

    # close()         # call this function upon exit
    def close(self):  # cleanup (cancel scan, end thread, close stage)
        self.cancelScan()   # abort scan (if currenty scanning)
        self.sleep()        # wait for stop command to register
        self.keep_running = False   # terminate thread
        if self.useEwStage:
            self.ew.close()

    def cancelScan(self):  # stop long movements (homing or scanning)
        self.queue = []
        self.cancelSignal = True
        
    def moveBwd(self):
        if self.rotDisc.counter <= 0:
            return
        actTime=time.time()             
        self.rotDisc.counter=self.rotDisc.counter-1
        self.rotDisc.moveStep(self.steps_per_move)
        self.sleep(actTime)
        self.progress.emit(self.rotDisc.counter)

    def moveFwd(self):
        if self.rotDisc.counter >= self.num_moves:
            return
        actTime=time.time()             
        self.rotDisc.counter=self.rotDisc.counter+1
        self.rotDisc.moveStep(self.steps_per_move)
        self.sleep(actTime)
        self.progress.emit(self.rotDisc.counter)

    def sleep(self, lastTime=None):
        actTime=time.time()
        if lastTime is None:
            lastTime = actTime
        while ((actTime-lastTime)<=self.delay):  # 2 sekunden einschwingzeit
            time.sleep(0.01)
            actTime=time.time()
        
    def moveHome(self):
        self.cancelSignal = False
        while self.rotDisc.counter>0:
            if self.cancelSignal:
                print('Stop homing. (Thread still running.)', flush=True)
                break
            self.moveBwd()
    
    def startScan(self):
        self.moveHome()
        while self.rotDisc.counter<self.num_moves:
            self.performAction()
            if self.cancelSignal:
                print('Stop scan. (Thread still running.)', flush=True)
                break
            self.moveFwd()
        self.performAction()

    # perform some action at every position of the scan process
    def performAction(self):
        # Implement your image acquisition routine here:
        print(f"Acquiring image {self.rotDisc.counter} of {self.num_moves}", flush=True)
        pass
        
    def command(self, commandStr=None, args=None):
        if commandStr is None:
            return
        
        if not self.keep_running:
            print("Thread isn't running. Ignoring command.", flush=True)
            return
        
        self.listening.emit(False)
        print(f'Adding to command: {commandStr}', flush=True)
        self.queue.append((commandStr, args))

    def run(self):
        print("=== Worker thread started ===", flush=True)
        self.keep_running = True
        self.cancelSignal = False
        while self.keep_running:
            if self.queue:
                commandStr, args = self.queue.pop(0)
                print(f'Command received: {commandStr}', flush=True)
                try:
                    if args is None:
                        self.execute(commandStr)
                    else:
                        self.execute(commandStr, *args)
                except Exception as err:
                    print("ERROR:", flush=True)
                    print(err, flush=True)
                    self.keep_running = False
            else:
                pass
                # self.listening.emit(True)
            time.sleep(0.01)
                        
        print("=== Worker thread terminated ===", flush=True)
    
    def execute(self, commandStr, args=None):
        print(f'executing command: {commandStr}', flush=True)
        if commandStr == "home":
            self.moveHome()
        elif commandStr == "bwd":
            self.moveBwd()
        elif commandStr == "fwd":
            self.moveFwd()
        elif commandStr == "scan":
            self.startScan()
        else:
            print(f"Unknown command: {commandStr}", flush=True)
        self.commandFinished.emit()
        if not self.queue:
            self.listening.emit(True)
        
    def reportFinished(self):
        print("Finished.", flush=True)

    def reportProgress(self, i):
        print(f"Progress: pos={i}", flush=True)

    def reportListening(self, isBusy):
        print(f"Listening {isBusy}.", flush=True)


# %%
import cv2 as cv
global camera
def acquireImage(imageNumber = None):
    global camera
    try:
        ret, frame = camera.read()
    except:
        camera = cv2.VideoDevice(0)
        if not camera.isOpened():
            print("ERROR: could not open camera")
        ret, frame = camera.read()    
    if not ret:
        return
    if imageNumber is not None:
        fname = f"img{imageNumber:04d}.png"
    else:
        fname = "img____.png"   # always overwrite the same file
    cv.imwwrite(fname, frame)
    
# %%
if __name__ == '__main__':
    
    ew = EposWrapper()
    try:
        ew.open()
        rotDisc = rotDisc()
        rotDisc.init(ew)
    except:
        print("Hardware not found. Using dummy stage.")
        rotDisc = rotDiscDummy()
        
    startTime=time.time()
    lastTime=startTime
    
    steps_per_revolution = rotDisc.resEncoder   # 2048 steps in 360°
    
    steps_per_move = 8  # muss 2^n sein
    num_moves = steps_per_revolution // steps_per_move
    assert(num_moves*steps_per_move == steps_per_revolution)
        
    while rotDisc.counter<num_moves:
         actTime=time.time()
         
         if ((actTime-lastTime)>2):  # 2 sekunden einschwingzeit
             print(f"step {rotDisc.counter:4d} of {num_moves}")
             rotDisc.counter=rotDisc.counter+1
             rotDisc.moveStep(steps_per_move)
             lastTime=actTime            

    rotDisc.close()
        